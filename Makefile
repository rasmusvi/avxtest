FLAGS =

all: AVX512test.x AVXtest.x SSEtest.x AVX512test_oneop.x AVXtest_oneop.x SSEtest_oneop.x
.PHONY: all

SSEtest.x: %.x: %.o data.o
	ld -o $@ $^

AVXtest.x: %.x: %.o data.o
	ld -o $@ $^

AVX512test.x: %.x: %.o data.o
	ld -o $@ $^

AVX512test.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)

AVXtest.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)

SSEtest.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)




SSEtest_oneop.x: %.x: %.o data.o
	ld -o $@ $^

AVXtest_oneop.x: %.x: %.o data.o
	ld -o $@ $^

AVX512test_oneop.x: %.x: %.o data.o
	ld -o $@ $^

AVX512test_oneop.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)

AVXtest_oneop.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)

SSEtest_oneop.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)

data.o: %.o: src/%.s Makefile
	as -o $@ $< $(FLAGS)




CLEAN =
CLEAN += SSEtest.x
CLEAN += AVXtest.x
CLEAN += AVX512test.x
CLEAN += SSEtest.o
CLEAN += AVXtest.o
CLEAN += AVX512test.o


CLEAN += SSEtest_oneop.x
CLEAN += AVXtest_oneop.x
CLEAN += AVX512test_oneop.x
CLEAN += SSEtest_oneop.o
CLEAN += AVXtest_oneop.o
CLEAN += AVX512test_oneop.o


CLEAN += data.o
clean:
	rm -f $(CLEAN)
.PHONY: clean
