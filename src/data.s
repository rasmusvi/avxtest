	.type	a0,@object                   
	.section	.rodata,"a",@progbits
	.globl	a0
	.p2align	3, 0x0
a0:
	.quad	0x425ac14dfd7de588           
	.size	a0, 8

	.type	a1,@object                   
	.globl	a1
	.p2align	3, 0x0
a1:
	.quad	0x4230df03601615e8           
	.size	a1, 8

	.type	a2,@object                   
	.globl	a2
	.p2align	3, 0x0
a2:
	.quad	0x4214f3e00b99b306           
	.size	a2, 8

	.type	a3,@object                   
	.globl	a3
	.p2align	3, 0x0
a3:
	.quad	0x41d84621c8d80267           
	.size	a3, 8

	.type	a4,@object                   
	.globl	a4
	.p2align	3, 0x0
a4:
	.quad	0x41a6da4f85f2dd20           
	.size	a4, 8

	.type	a5,@object                   
	.globl	a5
	.p2align	3, 0x0
a5:
	.quad	0x41590a0ce9ab57ad           
	.size	a5, 8

	.type	a6,@object                   
	.globl	a6
	.p2align	3, 0x0
a6:
	.quad	0x4113af7e092f8254           
	.size	a6, 8

	.type	b0,@object                   
	.globl	b0
	.p2align	3, 0x0
b0:
	.quad	0x425ac14dfd7de515           
	.size	b0, 8

	.type	b1,@object                   
	.globl	b1
	.p2align	3, 0x0
b1:
	.quad	0x424a45b5ae5f3e8d           
	.size	b1, 8

	.type	b2,@object                   
	.globl	b2
	.p2align	3, 0x0
b2:
	.quad	0x42281a1b56a62ddd           
	.size	b2, 8

	.type	b3,@object                   
	.globl	b3
	.p2align	3, 0x0
b3:
	.quad	0x41fb09ea7049e71e           
	.size	b3, 8

	.type	b4,@object                   
	.globl	b4
	.p2align	3, 0x0
b4:
	.quad	0x41c447bface2fb73           
	.size	b4, 8

	.type	b5,@object                   
	.globl	b5
	.p2align	3, 0x0
b5:
	.quad	0x4184aaf93aff672c           
	.size	b5, 8

	.type	b6,@object                   
	.globl	b6
	.p2align	3, 0x0
b6:
	.quad	0x413b059974ba83c7           
	.size	b6, 8

	.type	b7,@object                   
	.globl	b7
	.p2align	3, 0x0
b7:
	.quad	0x40e18c08c0af116e           
	.size	b7, 8

	.type	b8,@object                   
	.globl	b8
	.p2align	3, 0x0
b8:
	.quad	0xc06a79e88715496c           
	.size	b8, 8

	.type	b9,@object                   
	.globl	b9
	.p2align	3, 0x0
b9:
	.quad	0x3ff0000000000000           
	.size	b9, 8
