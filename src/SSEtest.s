.globl _start
.text
_start:
#  vmovddup	a0(%rip), %xmm0 
#  vmovddup	a1(%rip), %xmm1 
#  vmovddup	a2(%rip), %xmm2 
#  vmovddup	a3(%rip), %xmm3 
#  vmovddup	a4(%rip), %xmm4 
#  vmovddup	a5(%rip), %xmm5 
#  vmovddup	a6(%rip), %xmm6 
#  vmovddup	b0(%rip), %xmm7 
#  vmovddup	b1(%rip), %xmm8 
#  vmovddup	b2(%rip), %xmm9 
#  vmovddup	b3(%rip), %xmm10
#  vmovddup	b4(%rip), %xmm11
#  vmovddup	b5(%rip), %xmm12
#  vmovddup	b6(%rip), %xmm13
#  vmovddup	b7(%rip), %xmm14
#  vmovddup	b8(%rip), %xmm15
#  vmovddup	b9(%rip), %xmm16
  
  xor %edx, %edx
outerloop:
  vmovapd	%xmm17, %xmm18
  vfmadd213pd	%xmm5, %xmm6, %xmm18  
  vfmadd213pd	%xmm4, %xmm17, %xmm18 
  vfmadd213pd	%xmm3, %xmm17, %xmm18 
  vfmadd213pd	%xmm2, %xmm17, %xmm18 
  vfmadd213pd	%xmm1, %xmm17, %xmm18 
  vfmadd213pd	%xmm0, %xmm17, %xmm18 
  vmovapd	%xmm17, %xmm19
  vfmadd213pd	%xmm15, %xmm16, %xmm19
  vfmadd213pd	%xmm14, %xmm17, %xmm19
  vfmadd213pd	%xmm13, %xmm17, %xmm19
  vfmadd213pd	%xmm12, %xmm17, %xmm19
  vfmadd213pd	%xmm11, %xmm17, %xmm19
  vfmadd213pd	%xmm10, %xmm17, %xmm19
  vfmadd213pd	%xmm9, %xmm17, %xmm19 
  vfmadd213pd	%xmm8, %xmm17, %xmm19 
  vfmadd213pd	%xmm7, %xmm17, %xmm19 
  vdivpd	%xmm19, %xmm18, %xmm18
  inc %edx
  cmp $400000000, %edx
  jne outerloop
  movq $60, %rax
  syscall
