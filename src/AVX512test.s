.globl _start
.text
_start:
#  vbroadcastsd	a0(%rip), %zmm0 
#  vbroadcastsd	a1(%rip), %zmm1 
#  vbroadcastsd	a2(%rip), %zmm2 
#  vbroadcastsd	a3(%rip), %zmm3 
#  vbroadcastsd	a4(%rip), %zmm4 
#  vbroadcastsd	a5(%rip), %zmm5 
#  vbroadcastsd	a6(%rip), %zmm6 
#  vbroadcastsd	b0(%rip), %zmm7 
#  vbroadcastsd	b1(%rip), %zmm8 
#  vbroadcastsd	b2(%rip), %zmm9 
#  vbroadcastsd	b3(%rip), %zmm10
#  vbroadcastsd	b4(%rip), %zmm11
#  vbroadcastsd	b5(%rip), %zmm12
#  vbroadcastsd	b6(%rip), %zmm13
#  vbroadcastsd	b7(%rip), %zmm14
#  vbroadcastsd	b8(%rip), %zmm15
#  vbroadcastsd	b9(%rip), %zmm16
  
  xor %edx, %edx
outerloop:
  vmovapd	%zmm17, %zmm18
  vfmadd213pd	%zmm5, %zmm6, %zmm18  
  vfmadd213pd	%zmm4, %zmm17, %zmm18 
  vfmadd213pd	%zmm3, %zmm17, %zmm18 
  vfmadd213pd	%zmm2, %zmm17, %zmm18 
  vfmadd213pd	%zmm1, %zmm17, %zmm18 
  vfmadd213pd	%zmm0, %zmm17, %zmm18 
  vmovapd	%zmm17, %zmm19
  vfmadd213pd	%zmm15, %zmm16, %zmm19
  vfmadd213pd	%zmm14, %zmm17, %zmm19
  vfmadd213pd	%zmm13, %zmm17, %zmm19
  vfmadd213pd	%zmm12, %zmm17, %zmm19
  vfmadd213pd	%zmm11, %zmm17, %zmm19
  vfmadd213pd	%zmm10, %zmm17, %zmm19
  vfmadd213pd	%zmm9, %zmm17, %zmm19 
  vfmadd213pd	%zmm8, %zmm17, %zmm19 
  vfmadd213pd	%zmm7, %zmm17, %zmm19 
  vdivpd	%zmm19, %zmm18, %zmm18
  inc %edx
  cmp $100000000, %edx
  jne outerloop
  movq $60, %rax
  syscall
